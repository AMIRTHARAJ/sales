package com.aarria.retail.sales.messaging.receiver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.aarria.retail.sales.model.Event;

@Component
public class EventListener {

	private Logger logger = LoggerFactory.getLogger(EventListener.class);

	@JmsListener(destination = "QUEUE.EVENTS.WORLD")
	public void receiveMessageEvents(Event event) {
		System.out.println("Received event in World Events Listener " + event);
		logger.info("Received event in World Events Listener " + event);
	}
}
