package com.aarria.retail.sales.messaging.receiver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.aarria.retail.sales.messaging.request.dto.RateResponseDto;
import com.aarria.retail.sales.util.Queue;

@Component
public class ProductRatingResponseListener {

	private Logger LOGGER = LoggerFactory.getLogger(ProductRatingResponseListener.class);

	@JmsListener(destination = Queue.RATES_RESPONSE_QUEUE)
	public void receiveProductRatingRequest(RateResponseDto rateResponse) {
		LOGGER.info("Received rating response for " + rateResponse);
		
	}

}
