package com.aarria.retail.sales.messaging.receiver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.aarria.retail.sales.messaging.producer.ProductDetailsResponseProducer;
import com.aarria.retail.sales.messaging.request.dto.RateResponseDto;
import com.aarria.retail.sales.messaging.request.dto.RatesRequestDto;
import com.aarria.retail.sales.service.ProductRatingService;
import com.aarria.retail.sales.util.Queue;

@Component
public class ProductRatingRequestListener {

	private Logger LOGGER = LoggerFactory.getLogger(ProductRatingRequestListener.class);

	private ProductRatingService productRatingService;
	private ProductDetailsResponseProducer productDetailsResponseProducer;

	@Autowired
	public ProductRatingRequestListener(ProductRatingService productRatingService,
			ProductDetailsResponseProducer productDetailsResponseProducer) {
		this.productRatingService = productRatingService;
		this.productDetailsResponseProducer = productDetailsResponseProducer;
	}

	@JmsListener(destination = Queue.RATES_REQUEST_QUEUE)
	public void receiveProductRatingRequest(RatesRequestDto rateRequest) {
		LOGGER.info("Received rating request for " + rateRequest);

		// Receive, Get, Send
		RateResponseDto response = productRatingService.getProductRating(rateRequest);
		productDetailsResponseProducer.publishRates(response);

	}

}
