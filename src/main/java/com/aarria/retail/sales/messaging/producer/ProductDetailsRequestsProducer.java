package com.aarria.retail.sales.messaging.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.aarria.retail.sales.messaging.request.dto.RatesRequestDto;
import com.aarria.retail.sales.util.Queue;

@Component
public class ProductDetailsRequestsProducer {

	private Logger LOGGER = LoggerFactory.getLogger(ProductDetailsRequestsProducer.class);

	@Autowired
	private JmsTemplate jmsTemplate;

	public void sendProductRatingRequest(Long productId) {
		LOGGER.info("Requesting rating for product " + productId);
		jmsTemplate.convertAndSend(Queue.RATES_REQUEST_QUEUE, new RatesRequestDto(productId));
	}

}
