package com.aarria.retail.sales.messaging.request.dto;

public class RatesRequestDto {

	private Long productId;

	public RatesRequestDto() {
		super();
	}

	public RatesRequestDto(Long productId) {
		super();
		this.productId = productId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "RatesRequestDto [productId=" + productId + "]";
	}

	
	
}
