package com.aarria.retail.sales.messaging.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.aarria.retail.sales.messaging.request.dto.RateResponseDto;
import com.aarria.retail.sales.util.Queue;

@Component
public class ProductDetailsResponseProducer {

	private Logger LOGGER = LoggerFactory.getLogger(ProductDetailsResponseProducer.class);

	@Autowired
	private JmsTemplate jmsTemplate;

	public void publishRates(RateResponseDto response) {
		LOGGER.info("Started publishing the rating response for " + response);
		jmsTemplate.convertAndSend(Queue.RATES_RESPONSE_QUEUE, response);
	}
}
