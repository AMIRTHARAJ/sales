package com.aarria.retail.sales.messaging.request.dto;

public class RateResponseDto {

	private Long productId;
	private Double productRating;

	public RateResponseDto() {
		super();
	}

	public RateResponseDto(Long productId, Double productRating) {
		super();
		this.productId = productId;
		this.productRating = productRating;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Double getProductRating() {
		return productRating;
	}

	public void setProductRating(Double productRating) {
		this.productRating = productRating;
	}

	@Override
	public String toString() {
		return "RateResponseDto [productId=" + productId + ", productRating=" + productRating + "]";
	}

}
