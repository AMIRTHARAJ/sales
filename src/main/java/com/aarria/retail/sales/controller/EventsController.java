package com.aarria.retail.sales.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aarria.retail.sales.service.EventsService;

@RestController
@RequestMapping("/events")
public class EventsController {

	private EventsService eventsService;

	@Autowired
	EventsController(EventsService eventsService) {
		this.eventsService = eventsService;
	}

	@GetMapping("/publish/{count}/exception/{exceptionIteration}")
	public String publish(@PathVariable("count") int count,
			@PathVariable("exceptionIteration") int exceptionIteration) {
		eventsService.publishEvents(count, exceptionIteration);
		return "Messages started publishing";
	}

}
