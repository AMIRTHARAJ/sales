package com.aarria.retail.sales.model;

import java.io.Serializable;

public class Event implements Serializable {

	private static final long serialVersionUID = -295422703255886286L;

	private Long eventId;
	private String name;
	private String description;

	public Event() {
		super();
	}

	public Event(Long eventId, String name, String description) {
		this.eventId = eventId;
		this.name = name;
		this.description = description;

	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Event [eventId=" + eventId + ", name=" + name + ", description=" + description + "]";
	}

}
