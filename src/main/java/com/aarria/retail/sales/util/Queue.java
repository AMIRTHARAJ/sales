package com.aarria.retail.sales.util;

public class Queue {

	public static final String RATES_REQUEST_QUEUE = "RATES.REQUEST.QUEUE";
	public static final String RATES_RESPONSE_QUEUE = "RATES.RESPONSE.QUEUE";
}
