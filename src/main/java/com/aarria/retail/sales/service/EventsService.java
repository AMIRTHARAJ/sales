package com.aarria.retail.sales.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.aarria.retail.sales.model.Event;

@Service
public class EventsService {

	private Logger logger = LoggerFactory.getLogger(EventsService.class);

	@Autowired
	JmsTemplate jmsTemplate;

	public void publishEvents(int count, int exceptionIteration) {
		logger.info("Entered into publishing method..");
		Runnable r = () -> {
			for (long i = 0; i < count; i++) {

				if (i == exceptionIteration) {
					throw new RuntimeException("Sorry.. something went wrong..");
				}

				String message = "Message " + i;
				Event event = new Event(i, String.valueOf(i), message);
				jmsTemplate.convertAndSend("QUEUE.EVENTS.WORLD", event);

				logger.info("Published message " + i);
			}
		};
		logger.info("About to start the thread..");
		new Thread(r).start();
	}
}
