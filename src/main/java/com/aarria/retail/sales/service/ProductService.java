package com.aarria.retail.sales.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aarria.retail.sales.messaging.producer.ProductDetailsRequestsProducer;

@Service
public class ProductService {

	private Logger LOGGER = LoggerFactory.getLogger(ProductService.class);

	@Autowired
	private ProductDetailsRequestsProducer productRequestsProducer;

	public Double getProductRating(Long productId) {
		productRequestsProducer.sendProductRatingRequest(productId);
		// TODO sync the received message from product rating service :)
		return 0.0;
	}

}
