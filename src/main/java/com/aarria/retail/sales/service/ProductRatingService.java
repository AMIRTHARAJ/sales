package com.aarria.retail.sales.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.aarria.retail.sales.messaging.request.dto.RateResponseDto;
import com.aarria.retail.sales.messaging.request.dto.RatesRequestDto;

@Service
public class ProductRatingService {

	private Logger LOGGER = LoggerFactory.getLogger(ProductRatingService.class);

	public RateResponseDto getProductRating(RatesRequestDto rateRequest) {
		// TODO fetch from database
		Double productRating = 4.0;

		return new RateResponseDto(rateRequest.getProductId(), productRating);
	}

}
